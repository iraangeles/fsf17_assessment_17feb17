(function() {
 
 var RegisterApp = angular.module("RegisterApp",[]);

 var RegisterCtrl = function($http, $log){
    var registerCtrl = this;

    var createRegisterObject = function(Ctrl) {
      return ({
        emailadd : Ctrl.emailadd,
        password : Ctrl.password,
        name : Ctrl.name,
        address : Ctrl.address,
        contactno : Ctrl.contactno,
        birthday : Ctrl.birthday,
        gender : Ctrl.gender,
        country : Ctrl.country
      });  
    };

    var initObject = function(Ctrl){
        Ctrl.emailadd = "";
        Ctrl.password = "";
        Ctrl.name = "";
        Ctrl.address = "";
        Ctrl. contactno = "";
        Ctrl.birthday = "";
        Ctrl.gender = "";
        Ctrl.country = "";

    };

    // check Age
    var getAge = function(dob){
        var today = new Date();
        var yeartoday = today.getFullYear();

        var str = registerCtrl.birthday;
        var match = str.match(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
        var age = yeartoday - parseInt(match[3]) ;

        return age;

    };

    // check password 
    var checkPassword = function (pw){
        var validator = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
        return validator.test(pw);
    };


    // check mobile number
    var checkMobileNo = function(mobnum){
        var mob = /^[0-9]{8,10}$/;
        return mob.test(mobnum);
    };

    registerCtrl.status = "";

    
    registerCtrl.select = ["Singapore",
                            "Philippines",
                            "Malaysia",
                            "Indonesia",
                            "Thailand",
                            "India",
                            "Miyanmar",
                            "Cambodia",
                            "Vietnam",
                            "US",
                            "UK"];

    initObject(registerCtrl);

    registerCtrl.login = function() {


        if ( getAge(registerCtrl.birthday) < 18){
             registerCtrl.status = "Registrant must be at least 18 years old";
            //  console.log("good");
        }
        else if ( !checkPassword( registerCtrl.password ) )
            registerCtrl.status = "Password not strong enough, try again.";
        else if (!checkMobileNo( registerCtrl.contactno ))
            registerCtrl.status = "Contact number invalid, try again.";
        else {
           $http.get ("/register",  { 
               params : createRegisterObject(registerCtrl) 
            }).then( function(result) {
                console.log ("Sending details to server successfull.");
                registerCtrl.status = "Login is successful, thank you!"
                initObject(registerCtrl);
            }).catch( function(status) {
                registerCtrl.status = "Username or password are incorrect. Try again"
            });
       };


    };


 };


 RegisterApp.controller("RegisterCtrl", [ "$http", "$log", RegisterCtrl ]);


})();

