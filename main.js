// load libraries
var express = require("express");
var path = require("path");

// instantiate express
var app = express();

// set port
app.set("port",parseInt(process.argv[2]) || process.env.APP_PORT || 3000 );

// routes
app.use(express.static(path.join(__dirname,"public")));

// routes for Angular
app.use("/libs",express.static(path.join(__dirname,"bower_components")));

app.get("/register", function (req, resp) {

	// console.log("Registration Details: %s\n", JSON.stringify(req.query)) ;

	console.log("\nRegistration Details: \nName: %s\nAddress: %s\nContact No: %s\nDate of Birth: %s\nGender: %s\nCountry: %s\nEmail: %s\nPassword: %s\n", 
        req.query.name,req.query.address,req.query.contactno,req.query.birthday,
        req.query.gender,req.query.country,req.query.emailadd,req.query.password) ;

	resp.status(201).end();

});


app.listen(app.get("port"), function(){
    console.log("FSF 17 Assessment 17 Feb 2017\nRegistration Application started at %s on port %d", new Date(),app.get("port"));


});

